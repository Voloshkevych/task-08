package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		DOMController domController = new DOMController(xmlFileName);

		List<Flower> domList=domController.readXML();

		domList.sort(new Flower());
		System.out.println(domList);

		String outputXmlFile = "output.dom.xml";

		domController.writeXML(outputXmlFile,domList);

		SAXController saxController = new SAXController(xmlFileName);

		List<Flower> list = new ArrayList<>();
		list = saxController.readXML();
		System.out.println(list);

		list.sort(new Flower());
		System.out.println(list);

		outputXmlFile = "output.sax.xml";

		saxController.writeXML(outputXmlFile,list);

		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> staxList = staxController.readXML();
		System.out.println("STAX");
		System.out.println(staxList);

		staxList.sort(new Flower());

		outputXmlFile = "output.stax.xml";

		staxController.writeXML(outputXmlFile,staxList);
	}
}
