package com.epam.rd.java.basic.task8.controller;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	private List<Flower> list = new ArrayList<>();
	public List<Flower> readXML() throws ParserConfigurationException, IOException, SAXException {
		// Получение фабрики, чтобы после получить билдер документов.
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Запарсили XML, создав структуру Document. Теперь у нас есть доступ ко всем элементам, каким нам нужно.
		Document document = builder.parse(new File(xmlFileName));


		NodeList flowersList = document.getDocumentElement().getElementsByTagName("flower");
		//System.out.println(flowersList);
		for(int i=0; i< flowersList.getLength();i++){
			if(flowersList.item(i) instanceof Element){
				Node flower = flowersList.item(i);
				Flower flow = new Flower();
				//	System.out.println(flower);
				NodeList flowerChild = flower.getChildNodes();
				for(int j=0;j<flowerChild.getLength();j++){
					if(flowerChild.item(j) instanceof  Element){
						//	System.out.println("\t"+flowerChild.item(j));
						Node flowerNode = flowerChild.item(j);
						switch(flowerNode.getNodeName()){
							case "name": flow.name=flowerNode.getTextContent();break;
							case "soil": flow.soil=flowerNode.getTextContent();break;
							case "origin": flow.origin=flowerNode.getTextContent();break;
							case "multiplying": flow.multiplying=flowerNode.getTextContent();break;
							case "visualParameters":
								NodeList visualParametersList = flowerNode.getChildNodes();
								for(int k=0;k< visualParametersList.getLength();k++){
									if(visualParametersList.item(k) instanceof Element){
										Node visualParameterNode = visualParametersList.item(k);
										switch(visualParameterNode.getNodeName()){
											case "stemColour": flow.stemColour=visualParameterNode.getTextContent();break;
											case "leafColour": flow.leafColour=visualParameterNode.getTextContent();break;
											case "aveLenFlower":
												flow.aveLenFlower=visualParameterNode.getTextContent();
												flow.aveLenFlowerMeasure = visualParameterNode.getAttributes().item(0).getTextContent();
												break;

										}
									}
								}
								break;
							case "growingTips":
								NodeList growingTipsList = flowerNode.getChildNodes();
								for(int k=0;k< growingTipsList.getLength();k++){
									if(growingTipsList.item(k) instanceof Element){
										Node growingTipNode = growingTipsList.item(k);
										switch(growingTipNode.getNodeName()){
											case "tempreture":
												flow.tempreture=growingTipNode.getTextContent();
												flow.tempretureMeasure=growingTipNode.getAttributes().item(0).getTextContent();
												break;
											case "lighting":
												flow.lightRequiring=growingTipNode.getAttributes().item(0).getTextContent();
												break;
											case "watering":
												flow.watering=growingTipNode.getTextContent();
												flow.wateringMeasure = growingTipNode.getAttributes().item(0).getTextContent();
												break;

										}
									}
								}
								break;
						}
					}
				}
				//System.out.println(flow);
				list.add(flow);
			}
		}
		return list;
	}
	public void writeXML(String nameFile,List<Flower> list){
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			Element rootElement = doc.createElement("flowers");
			doc.setXmlStandalone(true);
			rootElement.setAttribute("xmlns","http://www.nure.ua");
			rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
			doc.appendChild(rootElement);

			for(Flower flower:list){
				Element flowerElement = doc.createElement("flower");
				rootElement.appendChild(flowerElement);

				Element nameElement = doc.createElement("name");
				nameElement.setTextContent(flower.name);
				flowerElement.appendChild(nameElement);

				Element soilElement = doc.createElement("soil");
				soilElement.setTextContent(flower.soil);
				flowerElement.appendChild(soilElement);

				Element originElement = doc.createElement("origin");
				originElement.setTextContent(flower.origin);
				flowerElement.appendChild(originElement);

				Element visualParametersElement = doc.createElement("visualParameters");
				flowerElement.appendChild(visualParametersElement);
				Element stemColourElement = doc.createElement("stemColour");
				stemColourElement.setTextContent(flower.stemColour);
				visualParametersElement.appendChild(stemColourElement);
				Element leafColourElement = doc.createElement("leafColour");
				leafColourElement.setTextContent(flower.leafColour);
				visualParametersElement.appendChild(leafColourElement);
				Element aveLenFlowerElement = doc.createElement("aveLenFlower");
				aveLenFlowerElement.setTextContent(flower.aveLenFlower);
				aveLenFlowerElement.setAttribute("measure",flower.aveLenFlowerMeasure);
				visualParametersElement.appendChild(aveLenFlowerElement);

				Element growingTipsElement = doc.createElement("growingTips");
				flowerElement.appendChild(growingTipsElement);
				Element tempretureElement = doc.createElement("tempreture");
				Element lightingElement = doc.createElement("lighting");
				Element wateringElement = doc.createElement("watering");
				growingTipsElement.appendChild(tempretureElement);
				growingTipsElement.appendChild(lightingElement);
				growingTipsElement.appendChild(wateringElement);
				tempretureElement.setAttribute("measure", flower.tempretureMeasure);
				tempretureElement.setTextContent(flower.tempreture);
				lightingElement.setAttribute("lightRequiring",flower.lightRequiring);
				wateringElement.setAttribute("measure", flower.wateringMeasure);
				wateringElement.setTextContent(flower.watering);

				Element multiplyingElement = doc.createElement("multiplying");
				multiplyingElement.setTextContent(flower.multiplying);
				flowerElement.appendChild(multiplyingElement);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(nameFile));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}


	}
}
